package com.android.pomagierzy.ballshakerhy18.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface BallDao {
    @Query("SELECT * FROM ball")
    LiveData<List<Ball>> getAll();

    @Query("SELECT * FROM ball WHERE is_caught = 0")
    List<Ball> getAllNotCaught();

    @Insert
    void insert(Ball ball);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ArrayList<Ball> balls);

    @Delete
    void delete(Ball ball);

    @Query("UPDATE ball SET is_caught = 1 WHERE number =:number")
    void markAsCaught(int number);
}
