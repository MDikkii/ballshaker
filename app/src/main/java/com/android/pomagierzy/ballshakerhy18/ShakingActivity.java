package com.android.pomagierzy.ballshakerhy18;

import android.content.Context;
import android.content.Intent;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.pomagierzy.ballshakerhy18.ballpocket.BallRepository;
import com.android.pomagierzy.ballshakerhy18.ballpocket.BallRepositoryImpl;
import com.android.pomagierzy.ballshakerhy18.geofence.GeofencingActivity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class ShakingActivity extends AppCompatActivity {

    public static final String GAINED_NUMBER = "Gained Number";

    public float timeToGetBallInMs = 3000;
    public float bigShakeAccelerationLimit = 5;


    private SensorManager mSensorManager;
    private float currentAcceleration;
    private float lastAcceleration;
    private float currentAccelerationNoGravity;
    private float avgAcceleration;

    private TextView accelerationTextView;
    private ProgressBar shakePowerBar;

    private LinkedList<Float> accelerationQueue;
    private LinearLayout ballsLayout;
    private ArrayList<View> balls;

    private long startBigShakeTime;
    private boolean bigShake = false;

    private LinkedList<Integer> shakedNumbers;
    private MediaPlayer rollingSound;
    private MediaPlayer gainedBallSound;

    private Handler handler = new Handler();

    private BallRepository ballRepository;

    private boolean finishedShaking = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shaking);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        currentAccelerationNoGravity = 0.00f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;
        lastAcceleration = SensorManager.GRAVITY_EARTH;

        this.accelerationTextView = (TextView) findViewById(R.id.accelerationTextView);
        this.shakePowerBar = (ProgressBar) findViewById((R.id.shakePowerBar));

        this.accelerationQueue = new LinkedList<>();
        this.ballsLayout = findViewById(R.id.ballsLayout);
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.release);
        this.rollingSound = MediaPlayer.create(this, R.raw.rolling);
        mp.start();
        rollingSound.setLooping(true);
        rollingSound.start();
        this.gainedBallSound = MediaPlayer.create(this, R.raw.gain);
        LoadBalls();
        shakedNumbers = new LinkedList<>();
        handler.post(volumeUpdater);

        this.ballRepository = new BallRepositoryImpl(getApplicationContext());
    }

    private void LoadBalls() {
        this.balls = new ArrayList<>();
        for (int i = 0; i < this.ballsLayout.getChildCount(); i++) {

            View nextChild = this.ballsLayout.getChildAt(i);
            this.balls.add(nextChild);
            nextChild.setVisibility(View.INVISIBLE);
        }
    }

    private Runnable volumeUpdater = new Runnable() {
        @Override
        public void run() {
            SetRollingVolume();
            handler.postDelayed(this, 250);
        }
    };

    private void SetRollingVolume() {
        int currentVolume = Math.round(avgAcceleration * 10);
        float maxVolume = bigShakeAccelerationLimit * 15;
        final float volume = (float) (1 - (Math.log(maxVolume - currentVolume) / Math.log(maxVolume)));
        this.rollingSound.setVolume(volume, volume);
    }

    private final SensorEventListener mSensorListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent se) {
            if(!finishedShaking) {
                float x = se.values[0];
                float y = se.values[1];
                float z = se.values[2];
                lastAcceleration = currentAcceleration;
                currentAcceleration = (float) Math.sqrt((double) (x * x + y * y + z * z));
                float delta = currentAcceleration - lastAcceleration;
                currentAccelerationNoGravity = Math.abs(currentAccelerationNoGravity * 0.9f + delta); // perform low-cut filter
                AddAccelerationToQueue();
                avgAcceleration = CountAverageAcceleration();
                accelerationTextView.setText(Float.toString(avgAcceleration));
                if (avgAcceleration > bigShakeAccelerationLimit) {
                    if (!bigShake) {
                        bigShake = true;
                        startBigShakeTime = System.currentTimeMillis();
                    }
                    if (IsLongShaking()) {
                        ShowNextBall(GetRandomBall());
                        bigShake = false;
                    }
                } else {
                    bigShake = false;
                }
                shakePowerBar.setProgress((int) avgAcceleration);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        this.rollingSound.setLooping(true);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        this.rollingSound.setLooping(false);
        super.onPause();
    }

    private void AddAccelerationToQueue() {
        if (this.accelerationQueue.size() > 50) {
            this.accelerationQueue.removeLast();
        }
        this.accelerationQueue.addFirst(this.currentAccelerationNoGravity);
    }

    private float CountAverageAcceleration() {
        float average = 0;
        for (float acc : this.accelerationQueue) {
            average += acc;
        }
        return average / this.accelerationQueue.size();
    }

    private boolean IsLongShaking() {
        return System.currentTimeMillis() - this.startBigShakeTime > this.timeToGetBallInMs;
    }

    private int GetRandomBall() {
        return new Random().nextInt(49) + 1;
    }

    private void ShowNextBall(int numberToShow) {
        for (int i = 0; i < balls.size(); i++) {
            if (balls.get(i).getVisibility() == View.INVISIBLE) {
                ((TextView) ((RelativeLayout) balls.get(i)).getChildAt(1)).setText(Integer.toString(numberToShow));
                this.gainedBallSound.start();
                balls.get(i).setVisibility(View.VISIBLE);
                shakedNumbers.addLast(numberToShow);
                break;
            }
        }
        if(this.shakedNumbers.size() == this.balls.size())
        {
            ActivateButtons();
        }
    }

    public class IntegerOnClickListener implements View.OnClickListener {
        int val;

        public IntegerOnClickListener(int val) {
            this.val = val;
        }

        @Override
        public void onClick(View v) {
        }

    };

    private void ActivateButtons() {
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.thanks);
        mp.start();
        this.finishedShaking = true;
        this.rollingSound.stop();

        for (int i = 0; i < balls.size(); i++) {
            balls.get(i).setOnClickListener(new IntegerOnClickListener(i) {
                @Override
                public void onClick(View v) {
                    ChooseBall(shakedNumbers.get(this.val));
                }
            });
        }
    }

    private void ChooseBall(int number) {
        this.ballRepository.updateBallStatus(number, true);
        Toast.makeText(this.getApplicationContext(), "Zdobyłeś kulę numer " + number + "!",
                Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(GAINED_NUMBER, number);
        startActivity(intent);
    }
}
