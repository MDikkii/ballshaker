package com.android.pomagierzy.ballshakerhy18.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface OpeningHourDao {
    @Query("SELECT * FROM opening_hours")
    List<OpeningHour> getAll();

    @Query("SELECT * FROM opening_hours WHERE game_area_id=:areaId")
    List<OpeningHour> getOpeningHoursForArea(int areaId);

    @Insert
    void insert(OpeningHour openingHour);

    @Delete
    void delete(OpeningHour openingHour);
}
