package com.android.pomagierzy.ballshakerhy18.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "opening_hours",
        foreignKeys = @ForeignKey(entity = GameArea.class,
                parentColumns = "id",
                childColumns = "game_area_id",
                onDelete = CASCADE))
public class OpeningHour {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "game_area_id")
    public int gameAreaId;

    @ColumnInfo(name = "day_of_week")
    public int dayOfWeek;

    @ColumnInfo(name = "opening_hour")
    public double openingHour;

    @ColumnInfo(name = "closing_hour")
    public double closingHour;

}
