package com.android.pomagierzy.ballshakerhy18.geofence;

public class Constants {
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 1000000; // 1000s
    public static final float GEOFENCE_RADIUS_IN_METERS = 1000;
}
