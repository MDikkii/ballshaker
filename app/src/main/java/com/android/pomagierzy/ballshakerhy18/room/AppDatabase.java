package com.android.pomagierzy.ballshakerhy18.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.concurrent.Executors;

@Database(entities = {Ball.class, GameArea.class, OpeningHour.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract BallDao ballDao();

    public abstract GameAreaDao gameAreaDao();

    public abstract OpeningHourDao openingHourDao();


    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "app_database")
                            .addCallback(new Callback() {
                                @Override
                                public void onCreate(@NonNull SupportSQLiteDatabase db) {
                                    super.onCreate(db);
                                    Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                                        @Override
                                        public void run() {


                                            ArrayList<Ball> balls = new ArrayList<>();

                                            for (int i = 0; i < 49; i++) {
                                                Ball ball = new Ball();
                                                ball.isCaught = false;
                                                ball.number = i + 1;
                                                balls.add(ball);
                                            }

                                            getDatabase(context).ballDao().insertAll(balls);
                                        }
                                    });
                                }
                            })
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
