package com.android.pomagierzy.ballshakerhy18;

import android.app.Application;

import com.android.pomagierzy.ballshakerhy18.room.AppDatabase;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        AppDatabase.getDatabase(getApplicationContext());


    }


}
