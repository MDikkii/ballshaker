package com.android.pomagierzy.ballshakerhy18.ballpocket;

class BallInPocket {
    boolean mIsCaught;
    int mNumber;

    public BallInPocket(int number, boolean isCaught) {
        mIsCaught = isCaught;
        mNumber = number;
    }
}
