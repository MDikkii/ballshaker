package com.android.pomagierzy.ballshakerhy18.ballpocket;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.pomagierzy.ballshakerhy18.R;
import com.android.pomagierzy.ballshakerhy18.room.Ball;

import java.util.ArrayList;

import static com.android.pomagierzy.ballshakerhy18.ballpocket.BallAdapter.BallType.CAUGHT;
import static com.android.pomagierzy.ballshakerhy18.ballpocket.BallAdapter.BallType.NOT_CAUGHT;

public class BallAdapter extends RecyclerView.Adapter<BallAdapter.BallViewHolder> {
    private int mLayoutId;

    enum BallType {
        CAUGHT,
        NOT_CAUGHT
    }

    private ArrayList<Ball> mBallList;

    public BallAdapter(ArrayList<Ball> ballList, int layoutId) {
        mBallList = ballList;
        mLayoutId = layoutId;
    }

    @NonNull
    @Override
    public BallViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(mLayoutId, parent, false);
        return new BallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BallViewHolder viewHolder, int position) {
        viewHolder.setContent(position + 1, getItemViewType(position));
    }

    @Override
    public int getItemViewType(int position) {
        return mBallList.get(position).isCaught ? CAUGHT.ordinal() : NOT_CAUGHT.ordinal();
    }

    @Override
    public int getItemCount() {
        return mBallList.size();
    }

    class BallViewHolder extends RecyclerView.ViewHolder {

        TextView numberTextView;
        ImageView ballImageView;

        public BallViewHolder(@NonNull View itemView) {
            super(itemView);
            numberTextView = itemView.findViewById(R.id.numberTextView);
            ballImageView = itemView.findViewById(R.id.ballImageView);
        }

        public void setContent(int number, int itemViewType) {
            numberTextView.setText(String.valueOf(number));
            ballImageView.setAlpha(itemViewType == CAUGHT.ordinal() ? 1f : 0.4f);
        }
    }
}
