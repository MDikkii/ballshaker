package com.android.pomagierzy.ballshakerhy18.geofence;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.android.pomagierzy.ballshakerhy18.R;
import com.android.pomagierzy.ballshakerhy18.ShakingActivity;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GeofenceTransitionsIntentService extends IntentService {

    private static final String CHANNEL_ID = "channel_id_1";

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    public static ArrayList<String> triggeredGeofenceIds = new ArrayList<>();

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        System.out.println(geofencingEvent.toString());
        if (geofencingEvent.hasError()) {
//            String errorMessage = GeofenceErrorMessages.getErrorString(this,
//                    geofencingEvent.getErrorCode());
            String errorMessage = "geofence transitions error";
            Log.e(TAG, errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            for(Geofence gf : triggeringGeofences)
            {
                Log.e("GEOFENCEID", gf.getRequestId());
                if (CheckIfAlreadyTriggered(gf)){
                    return;
                }
                else
                {
                    this.triggeredGeofenceIds.add(gf.getRequestId());
                }
            }

            System.out.println(triggeringGeofences);
            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(
//                    this,
                    geofenceTransition,
                    triggeringGeofences
            );

            // Send notification and log the transition details.
            sendNotification(geofenceTransitionDetails);
            Log.i(TAG, geofenceTransitionDetails);
        } else {
            // Log the error.
//            Log.e(TAG, getString(R.string.geofence_transition_invalid_type,
//                    geofenceTransition));
        }
    }

    private boolean CheckIfAlreadyTriggered(Geofence geofence)
    {
        return triggeredGeofenceIds.contains(geofence.getRequestId());
    }

    private String getGeofenceTransitionDetails(int geofenceTransition, List<Geofence> triggeringGeofences) {
        // TODO sound message
        String textA = "";
        switch (geofenceTransition) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                textA = "Entered";
                break;
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                textA = "Exited";
                break;
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                textA = "Stayed";
                break;
            default:
                break;
        }
        String textB = "";
        if (triggeringGeofences != null && !triggeringGeofences.isEmpty()) {
            Geofence geo = triggeringGeofences.get(0);
            textB = geo.toString();
        }
        return textA + " " + textB;
    }

    private void sendNotification(String geofenceTransitionDetails) {
        Intent intent = new Intent(this, ShakingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Uri sound = getSound();

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle("LOTTO")
                .setContentText(geofenceTransitionDetails)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                .setSound(sound)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        System.out.println("created notification with builder " + mBuilder);
        createNotificationChannel();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        MediaPlayer mp = MediaPlayer.create(this.getApplicationContext(), R.raw.welcome);
        mp.start();
// notificationId is a unique int for each notification that you must define
        int notificationId = (int) (Math.random() * 1000.0);
        notificationManager.notify(notificationId, mBuilder.build());
    }

    private Uri getSound() {
        String a = this.getApplicationContext().getPackageName();
        System.out.println(a);
        Uri path = Uri.parse("android.resource://" + a + "/" + R.raw.welcome);
//        Uri path = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        return path;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
//            channel.setSound(getSound(), getAudioAttributes());
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private AudioAttributes getAudioAttributes() {
//        AudioAttributes a = new AudioAttributes();
        return new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build();
    }
}
