package com.android.pomagierzy.ballshakerhy18.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface GameAreaDao {
    @Query("SELECT * FROM game_area")
    List<GameArea> getAll();

    @Insert
    void insert(GameArea gameArea);

    @Delete
    void delete(GameArea gameArea);
}
