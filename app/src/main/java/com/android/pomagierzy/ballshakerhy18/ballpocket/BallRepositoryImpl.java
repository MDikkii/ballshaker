package com.android.pomagierzy.ballshakerhy18.ballpocket;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import com.android.pomagierzy.ballshakerhy18.room.AppDatabase;
import com.android.pomagierzy.ballshakerhy18.room.Ball;
import com.android.pomagierzy.ballshakerhy18.room.BallDao;

import java.util.ArrayList;
import java.util.List;

public class BallRepositoryImpl implements BallRepository {

    private BallDao ballDao;
    private LiveData<List<Ball>> balls;

    public BallRepositoryImpl(Context context) {
        AppDatabase appDatabase = AppDatabase.getDatabase(context);
        ballDao = appDatabase.ballDao();
        balls = ballDao.getAll();
    }

    private ArrayList<BallInPocket> mBalls = new ArrayList<>();

    @Override
    public int getBallsCount() {
        return mBalls.size();
    }

    @Override
    public LiveData<List<Ball>> getBalls() {
        return balls;
    }

    @Override
    public void updateBallStatus(int number, boolean isCaught) {
        new UpdateAsyncTask(ballDao).execute(number);
    }

    @Override
    public void insert(Ball ball) {
        new InsertAsyncTask(ballDao).execute(ball);
    }

    private static class InsertAsyncTask extends AsyncTask<Ball, Void, Void> {

        private BallDao mAsyncTaskDao;

        InsertAsyncTask(BallDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Ball... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<Integer, Void, Void> {

        private BallDao mAsyncTaskDao;

        UpdateAsyncTask(BallDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.markAsCaught(params[0]);
            return null;
        }
    }
}
