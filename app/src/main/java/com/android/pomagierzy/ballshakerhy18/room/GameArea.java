package com.android.pomagierzy.ballshakerhy18.room;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "game_area")
public class GameArea {
    @PrimaryKey(autoGenerate = true)
    public int id;

    public double lat;
    public double lon;
    public int radius;


}
