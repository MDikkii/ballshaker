package com.android.pomagierzy.ballshakerhy18.ballpocket;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.pomagierzy.ballshakerhy18.R;
import com.android.pomagierzy.ballshakerhy18.room.Ball;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BallPocketActivity extends AppCompatActivity {

    RecyclerView ballRecyclerView;

    BallRepository ballRepository;

    ArrayList<Ball> list = new ArrayList<>();
    private BallAdapter ballAdapter;
/*
    @Override
    protected void onResume() {
        super.onResume();

        Random r = new Random();
        int i = r.nextInt(49);
        ballRepository.updateBallStatus(i, true);
    }
*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ball_pocket);

        ballRecyclerView = findViewById(R.id.ballRecyclerView);
        ballRecyclerView.setLayoutManager(new GridLayoutManager(this, 6));
        ballAdapter = new BallAdapter(list, R.layout.ball_entry);
        ballRecyclerView.setAdapter(ballAdapter);

        ballRepository = new BallRepositoryImpl(this);
        LiveData<List<Ball>> balls = ballRepository.getBalls();
        balls.observe(this, new Observer<List<Ball>>() {
            @Override
            public void onChanged(@Nullable List<Ball> balls) {
                if (balls != null) {
                    list = new ArrayList<>(balls);
                    ballAdapter = new BallAdapter(list, R.layout.ball_entry);
                    ballRecyclerView.setAdapter(ballAdapter);
                }

            }
        });



    }

    private ArrayList<BallInPocket> getBallList() {
        ArrayList<BallInPocket> balls = new ArrayList<>();
        for (int i = 0; i < 49; i++) {
            balls.add(new BallInPocket(i + 1, i % 2 == 0));
        }

        return balls;
    }


}
