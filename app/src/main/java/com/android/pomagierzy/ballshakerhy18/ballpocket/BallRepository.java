package com.android.pomagierzy.ballshakerhy18.ballpocket;

import android.arch.lifecycle.LiveData;

import com.android.pomagierzy.ballshakerhy18.room.Ball;

import java.util.List;

public interface BallRepository {
    int getBallsCount();
    LiveData<List<Ball>> getBalls();
    void updateBallStatus(int number, boolean isCaught);

    void insert(Ball ball);
}
